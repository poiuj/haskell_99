
import Data.Bits
import Numeric (showIntAtBase)
import Data.Char (intToDigit)

import Test.QuickCheck

grayHelper :: Int -> Int
grayHelper x = (x `xor` (x `shiftR` 1))

gray :: Int -> [String]
gray n = map (showHelper . grayHelper) [0..1 `shiftL` n - 1]
  where
    showHelper x = showIntAtBase 2 intToDigit x ""

-- tests

prop_correct n = let result = grayHelper n in
  n == result `xor` (n `shiftR` 1)

