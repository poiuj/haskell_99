module Task46 (and', or', nand', nor', xor', impl', equ') where

import Data.List (zipWith)
import Control.Monad
import Test.QuickCheck

and' :: Bool -> Bool -> Bool
and' = (&&)

or' :: Bool -> Bool -> Bool
or' = (||)

nand' :: Bool -> Bool -> Bool
nand' a b = not $ and' a b

nor' :: Bool -> Bool -> Bool
nor' a b = not $ or' a b

xor' :: Bool -> Bool -> Bool
xor' False False = False
xor' False True = True
xor' True False = True
xor' True True = False

impl' :: Bool -> Bool -> Bool
impl' False False = True
impl' False True = True
impl' True False = False
impl' True True = True

equ' :: Bool -> Bool -> Bool
equ' = (==)

infixl 3 `equ'`
infixl 4 `or'`
infixl 6 `and'`

bools :: [Bool]
bools = [False, True]

calcTable :: (Bool -> Bool -> Bool) -> [(Bool, Bool, Bool)]
calcTable fn = do
  x <- bools
  y <- bools
  return (x, y, (fn x y))

printTableLine :: (Bool, Bool, Bool) -> IO ()
printTableLine (x, y, z) = putStrLn $ show x ++ "\t" ++ show y ++ "\t" ++ show z

table :: (Bool -> Bool -> Bool) -> IO ()
table fn = mapM_ printTableLine (calcTable fn)
  
-- tests
a' :: Bool -> Bool -> Bool
a' x _ = x

b' :: Bool -> Bool -> Bool
b' _ y = y

fns :: [(Bool -> Bool -> Bool)]
fns = [a', b', and', or', nand', nor', xor', impl', equ']

generateFn :: Int -> Gen (Bool -> Bool -> Bool)
generateFn n | n <= 1 = elements [a', b']
             | otherwise = do
  fn <- elements fns
  fn1 <- generateFn (n-1)
  fn2 <- generateFn (n-1)
  return $ (\a b -> fn (fn1 a b) (fn2 a b))

newtype RandomFunction = RandomFunction { fromRandomFunction ::  (Bool -> Bool -> Bool)}

instance Arbitrary RandomFunction where
  arbitrary = sized (\size -> liftM RandomFunction (generateFn size))

instance Show RandomFunction where
  show _ = "some function..."

prop_correct fn = let results = calcTable (fromRandomFunction fn) in
  and $ map correctHelper results
  where
    correctHelper (x, y, z) = (fromRandomFunction fn) x y == z



