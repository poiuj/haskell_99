import Task46

import Control.Monad

bools :: [Bool]
bools = [False, True]

combinations :: Int -> [[Bool]]
combinations n | n <= 1 = [[x] | x <- bools]
               | otherwise = do
                 x <- bools
                 xs <- combinations (n-1)
                 return $ x:xs

calcTable :: Int -> ([Bool] -> Bool) -> [([Bool], Bool)]
calcTable n fn = do
  combination <- combinations n
  let result = fn combination
  return (combination, result)


showBools :: [Bool] -> String
showBools (x1:x2:[]) = show x1 ++ "\t" ++ show x2
showBools (x:xs) = show x ++ "\t" ++ showBools xs

tablen :: Int -> ([Bool] -> Bool) -> IO ()
tablen n fn = mapM_ printTableLine (calcTable n fn)
  where
    printTableLine (xs, r) = putStrLn $ showBools xs ++ "\t" ++ show r

-- tests


