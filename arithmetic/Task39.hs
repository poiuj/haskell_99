
module Task39 (primesR) where

import Task31 (isPrime)

primesR :: Int -> Int -> [Int]
primesR a b = filter isPrime [a..b]

