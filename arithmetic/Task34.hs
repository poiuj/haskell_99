
module Task34 (totient) where

totient :: Int -> Int
totient m = foldl totientInner 0 [1..m]
  where
    totientInner acc x | gcd m x == 1 = acc+1
                       | otherwise = acc
                                     
