
module Task35 (primeFactors) where

import Task31 (isPrime)

import Test.QuickCheck

primeFactors :: Int -> [Int]
primeFactors n = primeFactorsHelper n n []
  where
    primeFactorsHelper _ 1 acc | null acc = [1]
                               | otherwise = acc
    primeFactorsHelper rest i acc | (isPrime i) && (rest `mod` i == 0) =
      primeFactorsHelper (rest `div` i) i (i:acc)
                                  | otherwise = primeFactorsHelper rest (i-1) acc
    
    
-- tests

prop_product n = n > 0 ==> product (primeFactors n) == n

prop_primes n = n > 0 ==> all isPrime (primeFactors n)
