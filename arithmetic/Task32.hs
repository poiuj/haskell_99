
import Test.QuickCheck
import Task31 (isPrime)

myGCD :: Int -> Int -> Int
myGCD a b | b == 0 = abs a
          | otherwise = myGCD b (a `mod` b)
                        
-- tests

prop_gcd a b = myGCD a b == gcd a b
