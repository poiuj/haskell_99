module Task40 (goldbach) where 

import Task39 (primesR)

import Test.QuickCheck
import Task31 (isPrime)

goldbach :: Int -> (Int, Int)
goldbach n = let primes = primesR 2 n in
  head [(x,y) | x <- primes, y <- primes, x+y==n]  

-- tests

prop_prime n = n > 2 && even n ==> let (a, b) = goldbach n in
  isPrime a && isPrime b

prop_eq n = n > 2 && even n ==> let (a, b) = goldbach n in
  a + b == n
