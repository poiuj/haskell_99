
module Task31 (isPrime) where

isPrime :: Int -> Bool
isPrime n = primeHelper (floor (sqrt (fromIntegral n)))
  where
    primeHelper :: Int -> Bool
    primeHelper 1 = True
    primeHelper n' = if n `mod` n' == 0
                     then False
                     else primeHelper (n'-1)

