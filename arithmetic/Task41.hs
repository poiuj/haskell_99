import Task40 (goldbach)
import Task31 (isPrime)

import Test.QuickCheck

goldbachList :: Int -> Int -> [(Int, Int)]
goldbachList a b = let a' = round2 a in
  map goldbach [a', a'+2 .. b]

round2 :: Int -> Int
round2 n | even n = n
         | otherwise = n+1


goldbachList' :: Int -> Int -> Int -> [(Int, Int)]
goldbachList' a b barrier = filter helper (goldbachList a b)
  where
    helper (x, y) = x > barrier && y > barrier
    

-- tests

prop_prime a b = a > 2 && b > a ==> all (\(x,y)-> isPrime x && isPrime y) (goldbachList a b)

prop_eq a b = a > 2 && b > a ==> let evens = [round2 a, round2 a + 2..b] in
  and $ zipWith (\(a,b) n -> a+b == n) (goldbachList a b) evens
