
module Task36 (primeFactorsMult) where

import Data.List
import Control.Arrow

import Task35 (primeFactors)
import Task31 (isPrime)

import Test.QuickCheck

primeFactorsMult :: Int -> [(Int, Int)]
primeFactorsMult n = (map (head &&& length) . group . primeFactors) n
  
-- tests

prop_product n = n > 0 ==> product (map (\(a, b) -> (fromIntegral a) ** (fromIntegral b)) (primeFactorsMult n)) == fromIntegral n

prop_primes n = n > 0 ==> all (isPrime . fst) (primeFactorsMult n)
