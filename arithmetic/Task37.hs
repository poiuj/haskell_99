
import Task36 (primeFactorsMult)
import Task34 (totient)

import Test.QuickCheck

phi :: Int -> Int
phi n = product $ map phiHelper $ primeFactorsMult n
  where
    phiHelper :: (Int, Int) -> Int
    phiHelper (a, b) = (a-1) * (round (fromIntegral a ** fromIntegral (b - 1)))

-- tests

prop_totient n = n > 1 ==> phi n == totient n
