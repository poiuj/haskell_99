import Test.QuickCheck

myButLast :: [a] -> [a]
myButLast (x:[]) = []
myButLast (x:xs) = x : myButLast xs
myButLast _ = error "list can't be empty"

--- tests

prop_init xs = not (null xs) ==>
               myButLast xs == init xs




