
split :: [a] -> Int -> ([a], [a])
split xs n = innerSplit xs [] n

innerSplit xxs@(x:xs) acc n | n > 0 = innerSplit xs (x:acc) (n-1)
                            | otherwise = (reverse acc, xxs)
innerSplit _ acc _ = (reverse acc, [])

