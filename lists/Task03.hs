import Test.QuickCheck

elementAt :: [a] -> Int -> a
elementAt (x:xs) 1 = x
elementAt (x:xs) n | n > 1 = elementAt xs (n - 1)
                   | otherwise = error "n can't be less then 1"
elementAt _ _ = error "list is too short"

--- tests

prop_bang_bang xs a = a >= 1 && a < length xs ==>
                      elementAt xs a == xs !! (a - 1)

