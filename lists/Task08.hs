import Test.QuickCheck

compress :: Eq a => [a] -> [a]
compress (x1:x2:xs) | x1 == x2 = compress (x1:xs)
                    | otherwise = x1 : (compress (x2:xs))
compress xs = xs

