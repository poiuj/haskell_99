import Test.QuickCheck
import Test.QuickCheck.Monadic
import System.Random

rnd_select :: [a] -> Int -> IO [a]
rnd_select xs n = 
  if n <= 0 then
    return []
  else do
    rnd <- getStdRandom $ randomR (0, length xs - 1)
    let (beg, (e:end)) = splitAt rnd xs
    next_result <- rnd_select (beg ++ end) (n-1)
    return $ e : next_result

-- tests

prop_length xs n = n > 0 && n < length xs ==> monadicIO (test xs n)
  where
    test xs n = do result <- run $ rnd_select xs n
                   assert $ length result == n

prop_elems xs n = n > 0 && n < length xs ==> monadicIO (test xs n)
  where
    test xs n = do result <- run $ rnd_select xs n
                   assert $ and $ map (`elem` xs) result
                   
