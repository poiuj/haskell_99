
encode :: (Eq a) => [a] -> [(Int, a)]
encode xs = reverse $ innerEncode xs []

innerEncode :: (Eq a) => [a] -> [(Int, a)] -> [(Int, a)]
innerEncode (x:xs) buf@((n, a):bs) | x == a = innerEncode xs ((n+1, a):bs)
                                   | otherwise = innerEncode xs ((1, x):buf)
innerEncode (x:xs) _ = innerEncode xs [(1, x)]
innerEncode _ buf = buf

