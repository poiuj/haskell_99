import System.Random
import Test.QuickCheck
import Test.QuickCheck.Monadic

rnd_permu :: [a] -> IO [a]
rnd_permu xs = do
  if (null xs) then
    return []
    else do
      rnd_index <- randomRIO (0, length xs - 1)
      let (beg, e:end) = splitAt rnd_index xs
      next_result <- rnd_permu (beg ++ end)
      return $ e : next_result

-- tests

prop_length xs = monadicIO test
  where test = do result <- run $ rnd_permu xs
                  assert $ length xs == length result

prop_elems xs = monadicIO test
  where test = do result <- run $ rnd_permu xs
                  assert $ and $ map (countEq result) result
        countEq result x = count result x == count xs x
        count xs x = length $ filter (==x) xs
        
  
    
  
