
data EncodedData a = Single a
                   | Multiple Int a
                   deriving (Show)

decodeModified :: [EncodedData a] -> [a]
decodeModified xs = concat $ map expandData xs

expandData :: EncodedData a -> [a]
expandData (Single a) = [a]
expandData (Multiple n a) | n >1 = a : expandData (Multiple (n-1) a)
                          | otherwise = [a]

