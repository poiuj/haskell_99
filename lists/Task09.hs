import Test.QuickCheck

pack :: (Eq a) => [a] -> [[a]]
pack xs = reverse $ innerPack xs [] []

innerPack :: (Eq a) => [a] -> [a] -> [[a]] -> [[a]]
innerPack (x:xs) aas@(a:as) buf | x == a = innerPack xs (x:aas) buf
                                | otherwise = innerPack xs [x] (aas:buf)
innerPack (x:xs) _ _ = innerPack xs [x] []
innerPack _ as buf | null as = buf
                   | otherwise = as : buf
                                 
