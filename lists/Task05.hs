import Test.QuickCheck

myReverse :: [a] -> [a]
myReverse (x:xs) = myReverse xs ++ [x]
myReverse _ = []

-- tests

prop_reverse xs = myReverse xs == reverse xs

