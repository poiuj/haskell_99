import System.Random
import Test.QuickCheck
import Test.QuickCheck.Monadic

diff_select :: Int -> Int -> IO [Int]
diff_select n m = mapM (\_ -> randomRIO (0, m-1)) [1..n]
  
-- tests

prop_length n m = n > 0 ==> monadicIO $ test n m
  where
    test n m = do result <- run $ diff_select n m
                  assert $ length result == n

prop_elems n m = n > 0 && m > 0 ==> monadicIO $ test n m
  where
    test n m = do result <- run $ diff_select n m
                  assert $ all (<m) result
                  
