import Test.QuickCheck

myLast :: [a] -> a
myLast (x:[]) = x
myLast (x:xs) = myLast xs
myLast _ = error "Need at least one element"

--- tests

prop_last xs = not (null xs) ==>
               myLast xs == last xs
               

