
data EncodedData a = Single a
                   | Multiple Int a
                     deriving (Show)

encodeModified :: (Eq a) => [a] -> [EncodedData a]
encodeModified xs = reverse $ innerEncodeModified xs []

innerEncodeModified :: (Eq a) => [a] -> [EncodedData a] -> [EncodedData a]
innerEncodeModified (x:xs) buf@((Single a):bs) | x == a = innerEncodeModified xs ((Multiple 2 a) : bs)
                                                | otherwise = innerEncodeModified xs ((Single x) : buf)
innerEncodeModified (x:xs) buf@((Multiple n a):bs) | x == a = innerEncodeModified xs ((Multiple (n+1) a) : bs)
                                                    | otherwise = innerEncodeModified xs ((Single x) : buf)
innerEncodeModified (x:xs) _ = innerEncodeModified xs [Single x]
innerEncodeModified _ buf = buf

                  
