
repli :: [a] -> Int -> [a]
repli xs n = concat $ map (expandElem n) xs

expandElem :: Int -> a -> [a]
expandElem n x | n > 1 = x : expandElem (n-1) x
               | n == 1 = [x]
               | otherwise = []
                             
