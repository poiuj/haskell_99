import Test.QuickCheck

isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == reverse xs

-- tests

newtype Palindrome a = Palindrome { fromPalindrome :: [a] }
                       deriving (Show, Eq)

instance (Arbitrary a) => Arbitrary (Palindrome a) where
  arbitrary = do
    list <- arbitrary
    hasCentralElement <- arbitrary
    if hasCentralElement
      then do
      centralElement <- arbitrary
      return $ Palindrome (list ++ (centralElement : reverse list))
      else do
      return $ Palindrome (list ++ reverse list)


newtype NotPalindrome a = NotPalindrome { fromNotPalindrome :: [a] }
                          deriving (Show, Eq)

instance (Arbitrary a, Eq a) => Arbitrary (NotPalindrome a) where
  arbitrary = do
    palindrome <- arbitrary
    withNewElem <- pushNew $ fromPalindrome palindrome
    return $ NotPalindrome withNewElem

pushNew :: (Arbitrary a, Eq a) => [a] -> Gen [a]
pushNew xs = do
  e <- arbitrary
  if e `elem` xs
    then pushNew xs
    else return $ e : xs
    

prop_palindrome_true :: (Eq a) => Palindrome a -> Bool
prop_palindrome_true p = isPalindrome $ fromPalindrome p

prop_palindrome_false :: (Eq a) => NotPalindrome a -> Property
prop_palindrome_false p = (length $ fromNotPalindrome p) > 1 ==> not $ isPalindrome $ fromNotPalindrome p



