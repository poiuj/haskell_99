import Control.Monad
import Data.List (tails)

import Test.QuickCheck ((==>), quickCheck)

group :: [Int] -> [a] -> [[[a]]]
group (n:[]) xs = do
  (comb, rest) <- groupHelper n xs
  return comb:[]
group (n:ns) xs = do
  (comb, rest) <- groupHelper n xs
  otherCombs <- group ns rest
  return $ comb : otherCombs

groupHelper :: Int -> [a] -> [([a], [a])]
groupHelper 0 xs = [([], xs)]
groupHelper n xs = do
  index <- [0..length xs - 1]
  let (prev, (x:next)) = splitAt index xs
  (comb, rest) <- groupHelper (n-1) next
  return $ (x : comb, prev ++ rest)
  
                                     
-- tests

combs :: Int -> Int -> Int
combs n k = (product [n-k+1..n] `div` product [1..k])

prop_length ns xs = (all (>0) ns) && (not (null xs)) && (sum ns == length xs) ==>
                    length (group ns xs) == product (map (combs (length xs)) ns)
                    

