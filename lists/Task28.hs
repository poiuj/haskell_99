
import Data.List

import Test.QuickCheck

lsort :: [[a]] -> [[a]]
lsort = sortBy (\x y -> compare (length x) (length y))

lfsort :: [[a]] -> [[a]]
lfsort xs = sortBy (\x y -> compare (sizeCount (length x) xs) (sizeCount (length y) xs)) xs

sizeCount :: Int -> [[a]] -> Int
sizeCount n xs = count (\x -> length x == n) xs

count :: (a -> Bool) -> [a] -> Int
count fn = foldl step 0
  where
    step acc y | fn y = acc+1
               | otherwise = acc
                          

-- tests

prop_length xs = length xs == length (lsort xs)

prop_sorted xs = let result = lsort xs in sorted result
  where
    sorted (x1:x2:xs) | length x1 <= length x2 = sorted (x2:xs)
                      | otherwise = False
    sorted _ = True
    
                    

