import Test.QuickCheck

insertAt :: a -> [a] -> Int -> [a]
insertAt e (x:xs) pos | pos > 1 = x : insertAt e xs (pos-1)
                      | pos < 1 = error "Position is invalid"
                      | otherwise = e : x : xs
insertAt e _ pos | pos == 1 = [e]
                 | otherwise = error "Position is invalid"

-- tests

prop_e_is_elem e xs n = (n >= 1 && n <= (length xs)) ==>
                        e `elem` (insertAt e xs n)

prop_contain_original_list e xs n = (n >= 1 && n <= (length xs)) ==>
                                    let (beg, (y:end)) = splitAt n (insertAt e xs n) in
                                    beg ++ end == xs
                                    

