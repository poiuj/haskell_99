
dropEvery :: [a] -> Int -> [a]
dropEvery xs n0 = innerDropEvery xs n0
  where
    innerDropEvery (x:xs) n | n > 1 = x : innerDropEvery xs (n-1)
                            | otherwise = innerDropEvery xs n0
    innerDropEvery _ _ = []
    
