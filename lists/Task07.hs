import Test.QuickCheck

data NestedList a = Elem a
                    | List [NestedList a]
                    deriving (Show, Eq)

flatten :: NestedList a -> [a]
flatten (Elem a) = [a]
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
flatten (List _) = []

instance (Arbitrary a) => Arbitrary (NestedList a) where
  arbitrary = do
    n <- choose (0, 1) :: Gen Int
    case n of
      0 -> do
        elem <- arbitrary
        return $ Elem elem
      1 -> do
        elems <- arbitrary
        return $ List elems
    
listLength :: NestedList a -> Int
listLength (Elem _) = 1
listLength (List xs) = sum $ map listLength xs

prop_preserve_length :: NestedList a -> Bool
prop_preserve_length val = listLength val == (length $ flatten val) 

