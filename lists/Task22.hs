import Test.QuickCheck

range :: Int -> Int -> [Int]
range a b = rangeHelper a b
  where
    rangeHelper :: Int -> Int -> [Int]
    rangeHelper pos end | pos == end = [pos]
                        | otherwise = pos : rangeHelper (pos+1) end


-- tests

prop_correct_range a b = let d = b - a in
  d > 0 && d <= 10 ==>
  sum (range a b) == ((a + b) * (b - a + 1)) `div` 2
  
  

