
slice :: [a] -> Int -> Int -> [a]
slice xs a b = innerSlice xs [] 1
  where
    innerSlice (x:xs) acc n | n < a = innerSlice xs [] (n+1)
                            | n >= a && n <= b = innerSlice xs (x:acc) (n+1)
                            | otherwise = reverse acc
