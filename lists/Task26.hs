module Task26 (combinations) where

import Test.QuickCheck

combinations :: Int -> [a] -> [[a]]
combinations n xs | n == 0 = [[]]
                  | otherwise = do
                    index <- [0..length xs - 1]
                    let x = xs !! index
                    ys <- combinations (n-1) (drop (index+1) xs)
                    return $ x : ys

-- tests

prop_length k xs = k < length xs && length xs < 30 && k > 0  ==>
                   let result = combinations k xs
                       n = length xs in
                   (length result) == (product [k+1..n]) `div` (product [1..n-k])
                       
  
  
  
