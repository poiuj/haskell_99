import Test.QuickCheck

myLength :: [a] -> Int
myLength (x:xs) = myLength xs + 1
myLength _ = 0

-- tests

prop_length xs = myLength xs == length xs

