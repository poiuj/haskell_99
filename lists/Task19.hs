
rotate :: [a] -> Int -> [a]
rotate xs n0 =
  let n = n0 `mod` length xs
      firstPart = take n xs
      secondPart = drop n xs
  in secondPart ++ firstPart
     
